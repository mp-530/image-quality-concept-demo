FROM continuumio/anaconda3

WORKDIR /app

# Create the environment:
COPY environment.yml .
#RUN conda config --set restore_free_channel true
#RUN conda env create -f environment.yml
# RUN conda install python=3.7
RUN conda env create -f environment.yml

# Make RUN commands use the new environment:
SHELL ["conda", "run", "-n", "mp530", "/bin/bash", "-c"]

# Make sure the environment is activated:
#RUN echo "Make sure flask is installed:"
#RUN python -c "import flask"

# The code to run when container is started:
COPY . /app
ENTRYPOINT ["bash", "launchApp.sh" ]