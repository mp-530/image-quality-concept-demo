import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State, ClientsideFunction, ALL
from dash.exceptions import PreventUpdate
import plotly.express as px
import plotly.graph_objects as go
import plotly
import plotly.io as pio
from waitress import serve
import os
import time
from pathlib import Path
import json
import pandas as pd
import pydicom
import numpy as np
from scipy.ndimage import gaussian_filter
import imageio

#Global variables-------------------------------------------------------------
thisFilePath = Path(__file__)
path = thisFilePath.parent
ColorSequence = pio.templates["plotly_dark"].layout.colorway
path_im = path / 'assets' / 'ExampleImage.dcm'
#These are linear attenuation coefficients based on 64 keV X-rays
mu_water = 0.20003958
mu_air = 0.000218796
#Global variables-------------------------------------------------------------


#Helper functions-------------------------------------------------------------
def loadImage(path):
    ds = pydicom.dcmread(path)
    im = ds.pixel_array*ds.RescaleSlope + ds.RescaleIntercept
    return im, ds

def scaleContrast(im,scale):
    if scale == 1:
        return im
    
    #Convert to attenuation coefficients
    mu_x = (((mu_water-mu_air)*im/1000)+ mu_water)
    mu_x = mu_x**scale
    mu_w = mu_water**scale
    mu_a = mu_air**scale



    #Convert back to HU
    HU = 1000 * (mu_x - mu_w)/(mu_w - mu_a)
    return HU

def generateWhiteNoise(size):
    return np.random.normal(size = size)


def filterImage(im,sigma = 1):
    if sigma <= 0:
        return im
    else:
        return gaussian_filter(im, sigma = sigma)

def im2png(im,WW = 400, WL = 40):
    pmin = WL - WW/2
    pmax = WL + WW/2
    slope = 1/WW
    b = -slope*pmin
    im_w = im*slope + b
    im_w[im<pmin] = 0
    im_w[im >= pmax] = 1
    im_w = im_w*255
    im_w = im_w.astype(np.uint8)
    return im_w

def generateImage(contrast, resolution, noiseMag, texture, WL, WW):
    signal = scaleContrast(im,contrast)
    resolution = 3*(-resolution + 1)
    signal = filterImage(signal,sigma = resolution)
    texture = 2.5*(-texture + 1)
    noise = filterImage(im_noise, sigma = texture)
    noise = noiseMag * noise/noise.std()
    im_final = signal + noise
    #convert to png image
    im_out = im2png(im_final, WW = WW, WL = WL)
    imageio.imwrite(path / 'assets' / 'todisplay.png', im_out)
    return im_final, im_out

def generateHistogramPlots(im_final, im_out, WL, WW, range_final = (-1000, 2000)):
    #Generate the values for the window level function
    x = np.linspace(range_final[0], range_final[1],200)
    y = im2png(x,WW = WW, WL = WL)
    t1 = go.Scatter(
        x = x, y = y, 
        mode = 'lines',
        xaxis = 'x',
        yaxis = 'y',
        name = 'HU -> GL transformation'
    )
    #Generate the values for the image histogram
    histogram, bin_edges = np.histogram(im_final, bins=1000, range=(-1000, 2000))
    t2 = go.Scatter(
        x = bin_edges[0:-1], y = histogram,
        mode = 'lines',
        xaxis = 'x',
        yaxis = 'y2',
        name = 'HU Distribution'
    )
    #Generate the values for the displayed image histogram
    histogram, bin_edges = np.histogram(im_out, bins=256, range=(0,255))
    t3 = go.Scatter(
        y = bin_edges[0:-1], x = histogram,
        mode = 'lines',
        xaxis = 'x2',
        yaxis = 'y',
        name = 'GL Distribution'
    )

    #Generate the values for the colorscale
    t4 = go.Heatmap(
        z = np.arange(0,256),
        x = np.zeros(256), y = np.arange(0,256),
        xaxis = 'x3',
        yaxis = 'y',
        colorscale='gray',
        showscale=False 
    )

    data = [t1,t2,t3,t4]
    layout = go.Layout(
        xaxis = {'domain': [0,.45], 'title': 'CT Number [HU]'},
        yaxis = {'domain': [0, .45], 'title': 'Displayed gray level [GL]'},
        xaxis2 = {'domain': [.65, 1], 'showticklabels': False},
        yaxis2 = {'domain': [.5, 1], 'showticklabels': False},
        xaxis3 = {'domain': [.5, .6], 'showticklabels': False},
        template = 'plotly_dark',
    )
    fig = go.Figure(data=data, layout=layout)
    fig.update_layout(
        legend = dict(
            yanchor="top",
            y=0.99,
            xanchor="left",
            x=.46
        ),
    )
    fig.layout.coloraxis.showscale = False
    #fig.update_yaxes(showticklabels=False)
    fig.update_layout(margin=dict(l=0, r=0, t=0, b=5))
    return fig

#Helper functions-------------------------------------------------------------


#Make Dash app----------------------------------------------------------------
#Create the dash app instance
im, ds = loadImage(path_im)
im_noise = generateWhiteNoise(im.shape)
im_final, im_out = generateImage(1, .5, 100, .5, -350, 2000)

external_stylesheets = ['https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css',
                        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css']
app = dash.Dash(__name__, suppress_callback_exceptions=True,external_stylesheets=external_stylesheets)
app.title = 'MP 530 Image Quality Demo'

#Create the container for the app
layout = [
    html.H2('Image quality demo'),
    html.P('Move the sliders to see how the image changes with varying contrast, noise, and resolution characteristics'),
    html.P('Refresh the brower window to restore default settings'),
    html.Div(
        className = 'appContainer',
        children = [
            html.Div(
                className = 'imageContainer',
                children = [
                    html.Img(id = 'displayedImage', src = 'assets/todisplay.png?' + pd.Timestamp.today().strftime('%A %B %d, %I:%M:%S %p'), className = 'displayedImage'),
                ]
            ),
            html.Div(
                className = 'hSliderContainer',
                children = [
                    html.H4('Image characteristics'),
                    html.P('Contrast (<- Low, High ->)', className = 'SliderLabel'),
                    dcc.Slider(
                        id = 'contrastSlider', 
                        min = .1, 
                        max = 1.9, 
                        step = .01, 
                        className = 'horizontalSlider',
                        value = 1
                    ),
                    html.P('Spatial resolution (<- Poor, Good ->)', className = 'SliderLabel'),
                    dcc.Slider(
                        id = 'resolutionSlider', 
                        min = 0, 
                        max = 1, 
                        step = .01, 
                        className = 'horizontalSlider',
                        value = .5
                    ),
                    html.P('Noise magnitude (<- Low, High ->)', className = 'SliderLabel'),
                    dcc.Slider(
                        id = 'noiseSlider', 
                        min = 0, 
                        max = 200, 
                        step = 1, 
                        className = 'horizontalSlider',
                        value = 100
                    ),
                    html.P('Noise texture (<- Low freq., High freq. ->)', className = 'SliderLabel'),
                    dcc.Slider(
                        id = 'noiseTexturSlider', 
                        min = .1, 
                        max = 1, 
                        step = .01, 
                        className = 'horizontalSlider',
                        value = .55
                    ),
                    html.H4('Image display settings'),
                    html.P('Window level', className = 'SliderLabel', id = 'WLDisplay'),
                    dcc.Slider(
                        id = 'WLSlider', 
                        min = -1000, 
                        max = 2000, 
                        step = 10, 
                        className = 'horizontalSlider',
                        value = -350
                    ),
                    html.P('Window width', className = 'SliderLabel',id = 'WWDisplay'),
                    dcc.Slider(
                        id = 'WWSlider', 
                        min = 1, 
                        max = 2000, 
                        step = 1, 
                        className = 'horizontalSlider',
                        value = 2000
                    ),
                ]
            ),
            html.Div(
                className = 'histogramContainer',
                children = [
                    dcc.Graph(
                        id = 'HistogramCombined',
                        figure = generateHistogramPlots(im_final, im_out, -350, 2000),
                        style = {"width" : "100%"},
                        className = 'histogramPlots',
                    )
                ]
            )
        ]
    ),
    html.Button('Regenerate noise', id='regenerateNoise', n_clicks=0, className = 'generateNoiseButton'),

]
app.layout = html.Div(layout, className = 'appContent')
#Make Dash app----------------------------------------------------------------


#Register Callbacks-----------------------------------------------------------
@app.callback(
    [
        Output('displayedImage', 'src'),
        Output('WLDisplay', 'children'),
        Output('WWDisplay', 'children'),
        Output('HistogramCombined', 'figure'),
    ],
    [
        Input('contrastSlider','value'),
        Input('resolutionSlider','value'),
        Input('noiseSlider','value'),
        Input('noiseTexturSlider','value'),
        Input('WLSlider','value'),
        Input('WWSlider','value'),
        Input('regenerateNoise','n_clicks'),
    ]
)
def updateImage(contrast, resolution, noiseMag, texture, WL, WW, n_clicks):
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if 'regenerateNoise' in changed_id:
        global im_noise
        im_noise = generateWhiteNoise(im.shape)
    im_final, im_out = generateImage(contrast, resolution, noiseMag, texture, WL, WW)
    fig = generateHistogramPlots(im_final, im_out, WL, WW)

    return ['assets/todisplay.png?' + pd.Timestamp.today().strftime('%A %B %d, %I:%M:%S %p')], 'Window level: ' + str(WL), 'Window width: ' + str(WW), fig
#Register Callbacks-----------------------------------------------------------


#Run the server----------------------------------------------------------------
if __name__ == '__main__':
    print('Launching web app')
    serve(app.server, host='0.0.0.0', port=8050, threads = 8)