import pydicom
from pathlib import Path

#Get the path of this file
thisFilePath = Path(__file__)
path = thisFilePath.parent

#Load the dicom image
ds = pydicom.dcmread(path / 'assets' / 'ExampleImage_Original.dcm')

im = ds.pixel_array
mask = im==1024
im[mask]=24
im = im.transpose()
im = im[475:1400,:]
ds.Rows, ds.Columns = im.shape


ds.PixelData = im.tobytes()
ds.save_as(path / 'assets' / 'ExampleImage.dcm')