# image-quality-concept-demo

- Web app designed to demonstrate some basic image quality concepts.
- The app is built in Python using the [Plotly Dash](https://plotly.com/dash/) open-source package. 
- The source code includes files to help package the app into a [Docker image](https://www.docker.com/) which can be used to launch a local Docker container and run the app server.
- Once the app server is running (see setup below), you can access it through a web browser at [http://localhost:8050/](http://localhost:8050/).

## Setup instructions
### Method 1: Pre-built Docker image from Docke Hub (easiest)
1. Install [Docker Desktop](https://www.docker.com/get-started) on your machine.
2. Launch the Docker container:
    - Open a Terminal (Mac/Linux) or Powershell (Windows)
    - Run this command: `docker run -d -p 8050:8050 justinsolomon/image-quality-concept-demo:latest .`
    - The first time you run this Docker will look on your local machine to see if you have the `image-quality-concept-demo` Docker image. 
    - It will see that you don't have it and will download it, decompress it, and launch a Docker container based on that image.
    - This process involves a ~1.5 GB download which decompresses to ~4 GB. It will take some so just let it do its thing (go get a coffee).
    - Once its done it will print out a long alphanumeric string (e.g., `a32537...`) and return control to the command line. This string is the unique ID of the Docker container that was launched. Once this happens you can move onto the next step and use the web app.
3. Once the Docker container is running you can open a web browser to [http://localhost:8050/](http://localhost:8050/) to use the web app
    - Note that the web app will continue running in the background even if you close the browser window.
    - To stop it, run `docker kill $(docker ps -q)`
        - On linux you may need to run  `sudo docker kill $(sudo docker ps -q)`
    - This will stop all running Docker containers on your machine.
4. You can re-launch the container using the same `docker run...` command from step 2. 
    - The next time you launch it, it will boot up very quickly since it doesn't need to download anything.
    - The Docker image may be periodically updated over time. You can pull the latest version by running `docker pull justinsolomon/image-quality-concept-demo:latest` 
5. Here are some helpful tips/information:
    - This [tutorial](https://www.vikingcodeschool.com/web-development-basics/a-command-line-crash-course) may be helpful for those unfamiliar with using the command line.
    - When Docker Desktop is installed, it runs a "daemon" process in the background on your machine that is constantly listening for Docker commands. Be default its configured to boot up when the machine restarts. If you don't want it running the background you can kill it, but you'll have to restart it to run any Docker commands.
    - A Docker "Image" is basically a mini virtual machine (VM) running a Linux OS (the exact OS version depends on the specific image). It encapulsates all the code and dependencies needed for your code to run.
    - A Docker "Container" is "launched" based on a given Docker Image. You can launch multiple containers from the same image (this is like running multiple identical virtual computers at once).
    - Once a Docker Container is killed, its like throwing away that virtual computer.
    - The main reason why developers use Docker is that they can easily distribute their apps to many servers, and they don't have to worry about what operating system the server is running. Their Docker container acts like its own computer with its own OS and environement, and thus will work the same way everywhere.
    - This same fact also makes it ideal for a teacher to distribute a web app to their students, who may all be running different types of machines :) 
### Method 2: Build Docker image yourself (harder)
1. Install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) on your machine
    - A lot of computers come with Git installed out of the box. Especially Mac and Linux.
2. Clone this source code repository:
    - Open a Terminal (Mac/Linux) or Powershell (Windows)
    - Navigate to the directory where you want to save the source code: `cd path/to/code/directory/`
    - Clone the repository: `git clone https://gitlab.oit.duke.edu/mp-530/image-quality-concept-demo.git`
    - This will create a new directory called image-quality-concept-demo. Navigate to that directory: `cd image-quality-concept-demo`
3. Build the Docker image:
    - `docker build -t image-quality-concept-demo:latest .`
    - This will take some time since Docker will need to download the base image and install the dependent Python packages into it.
4. Launch the web app as a Docker container
    - Run `docker run -p 8050:8050 image-quality-concept-demo:latest .`